import { Component } from '@angular/core';

interface Element {
  [key: string]: boolean;
}

@Component({
  selector: 'elements',
  templateUrl: './elements.component.html'
})
export class ElementsComponent {
  element: Element = {
    Portfolio: true,
    'Communication Plan': true,
    'Artefact Management Plan': true,
    'Meeting Minutes': true
  };
}
