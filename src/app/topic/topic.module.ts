import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CommunicationComponent } from './communication/communication.component';
import { TeamRolesComponent } from './team-roles/team-roles.component';
import { ElementsComponent } from './elements/elements.component';
import { SecurityComponent } from './security/security.component';
import { TopicComponent } from './topic.component';
import { WhyComponent } from './why/why.component';

@NgModule({
  declarations: [
    CommunicationComponent,
    TeamRolesComponent,
    ElementsComponent,
    SecurityComponent,
    TopicComponent,
    WhyComponent
  ],
  imports: [CommonModule, NzTableModule, NzTabsModule]
})
export class TopicModule {}
