import { NzGridModule } from 'ng-zorro-antd/grid';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { RisksComponent } from './risks.component';

@NgModule({
  declarations: [RisksComponent],
  imports: [CommonModule, NzGridModule]
})
export class RisksModule {}
