import { NavigationEnd, Router } from '@angular/router';
import { Component } from '@angular/core';

import { pages, headerMap } from 'src/pages';

@Component({
  selector: 'root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  url = location.pathname;
  headerMap = headerMap;
  isCollapsed = false;
  pages = pages;

  constructor(router: Router) {
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.url = event.urlAfterRedirects;

        localStorage.setItem('path', this.url.slice(1, this.url.length));
        if (this.url != '/topic') localStorage.removeItem('index');
      }
    });

    const path = localStorage.getItem('path');
    if (path && path != this.url)
      router.navigate(['/', path], { skipLocationChange: true });
  }
}
