import { Component } from '@angular/core';

interface Reference {
  author: string;
  date: string;
  title: string;
  source: string;
  link: string;
}

@Component({
  selector: 'references',
  templateUrl: './references.component.html'
})
export class ReferencesComponent {
  references: Reference[] = [
    {
      author: '',
      date: '(2019, November 13).',
      title: 'AI Translators: The Future Of Language Learning?',
      source: 'Oxford House. Retrieved May 30, 2023.',
      link: 'https://reurl.cc/LA0oa4'
    },
    {
      author: 'Banik, I.',
      date: '(2023).',
      title: 'The Importance of Security in eLearning Platforms.',
      source: 'Muvi.',
      link: 'https://reurl.cc/DAvpqj'
    },
    {
      author: 'Betts, K.',
      date: '(2009).',
      title:
        'Lost in translation: Importance of effective communication in online education.',
      source: 'Online Journal of Distance Learning Administration, (12)2.',
      link: 'https://reurl.cc/94Z9DY'
    },
    {
      author: 'Burton, C.',
      date: '(2022, August 18).',
      title:
        '7 Top Challenges with Online Learning For Students (and Solutions).',
      source: 'Thinkific. Retrieved May 28, 2023.',
      link: 'https://reurl.cc/8jyVDg'
    },
    {
      author: 'Judi, H. M.',
      date: '(2022).',
      title:
        'Integrity and Security of Digital Assessment: Experiences in Online Learning.',
      source: 'Global Business & Management Research, Vol.14(1), 97 – 107.',
      link: 'https://reurl.cc/y7naD2'
    },
    {
      author: 'Mandela, N., & Tamm, S. ',
      date: '',
      title: '10 Biggest Disadvantages of E-Learning.',
      source: 'E-Student. Retrieved May 28, 2023.',
      link: 'https://reurl.cc/7kyqK5'
    },
    {
      author:
        'Metallidou, C., Psannis, K. E., Goudos, S., Sarigiannidis, P., & Ishibashi, Y.',
      date: '(2021, November).',
      title:
        'Communication and Security Issues in Online Learning during the COVID-19 Pandemic.',
      source:
        'In 2021 IEEE 9th International Conference on Information, Communication and Networks (ICICN) (pp. 538-544).',
      link: 'https://reurl.cc/y7naR2'
    },
    {
      author: 'Patel, H.',
      date: '(2021, July 27).',
      title:
        'Understanding Cyber Risks In eLearning | Education Cybersecurity Blog.',
      source: 'WPG Consulting. Retrieved May 28, 2023.',
      link: 'https://reurl.cc/p6mRaQ'
    },
    {
      author: 'Plitnichenko, L.',
      date: '',
      title: '10 Challenges of E-Learning during COVID-19.',
      source: 'Jellyfish.tech. Retrieved May 28, 2023.',
      link: 'https://reurl.cc/51oNRq'
    },
    {
      author: 'Venable, M.',
      date: '(July 22, 2015).',
      title: 'Make a personal connection in your online classroom.',
      source: '',
      link: 'https://reurl.cc/DAvpOj'
    },
    {
      author: 'Wilson, M., & Knerl, L.',
      date: '(2018, December 17).',
      title: 'How to Boost Your WiFi Signal at Home.',
      source: 'HP.com. Retrieved May 27, 2023.',
      link: 'https://reurl.cc/XEebQR'
    }
  ];

  open(url: string) {
    window.open(url, '_blank');
  }
}
