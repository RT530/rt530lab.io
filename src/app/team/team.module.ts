import { NzTableModule } from 'ng-zorro-antd/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TeamComponent } from './team.component';

@NgModule({
  declarations: [TeamComponent],
  imports: [CommonModule, NzTableModule]
})
export class TeamModule {}
