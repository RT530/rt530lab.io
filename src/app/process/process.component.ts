import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

interface TimelineItem {
  title: string;
  name: string;
  date: Date;
  color: 'blue' | 'red' | 'green' | 'gray';
}

interface Commit {
  title: string;
  committer_name: string;
  committed_date: string;
}

@Component({
  selector: 'process',
  templateUrl: './process.component.html'
})
export class ProcessComponent {
  timelineItems: TimelineItem[] = [];
  nameMap = new Map<string, string>();
  loading = true;

  headers = new HttpHeaders({
    Authorization: 'Bearer glpat-_XEiMcZZuB_QQmzAy1tk'
  });
  colors: ('blue' | 'red' | 'green' | 'gray')[] = [
    'blue',
    'red',
    'green',
    'gray'
  ];

  constructor(http: HttpClient) {
    this.nameMap.set('cjay01', 'Clyde Joshua Ferrera');
    this.nameMap.set('RT530', 'Ricky Tsai');
    this.colors.sort(() => 0.5 - Math.random());

    http
      .get<Commit[]>(
        'https://gitlab.com/api/v4/projects/45668890/repository/commits?per_page=1000',
        { headers: this.headers }
      )
      .subscribe((commits) => {
        let i = 0;
        for (const commit of commits) {
          const name = commit.committer_name;
          this.timelineItems.push({
            color: this.colors[i % this.colors.length],
            date: new Date(commit.committed_date),
            name: this.nameMap.get(name) || name,
            title: commit.title
          });
          i++;
        }
        this.timelineItems.push({
          color: this.colors[i % this.colors.length],
          date: new Date('2023-05-03T09:20:28.000+12:00'),
          name: 'Ricky Tsai',
          title: 'Upload'
        });
        this.loading = false;
      });
  }
}
