import { Component } from '@angular/core';

interface MemberRoles {
  name: string;
  roles: string[];
}

@Component({
  selector: 'team-roles',
  templateUrl: './team-roles.component.html'
})
export class TeamRolesComponent {
  memberRoles: MemberRoles[] = [
    {
      name: 'Ricky Tsai',
      roles: ['Website', 'Teaching', 'Repository Manager']
    },
    {
      name: 'Clyde Ferrera',
      roles: ['Website', 'Writing', 'Team Manager', 'Research']
    },
    {
      name: 'Lillian Tukor',
      roles: ['Website', 'Writing', 'Research']
    }
  ];
}
