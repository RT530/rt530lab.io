import { Component } from '@angular/core';

@Component({
  selector: 'topic',
  templateUrl: './topic.component.html'
})
export class TopicComponent {
  index = 0;

  constructor() {
    const index = localStorage.getItem('index');
    if (index) this.index = Number(index);
  }

  tabChange(index: number) {
    localStorage.setItem('index', String(index));
  }
}
