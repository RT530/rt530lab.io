import { NzGridModule } from 'ng-zorro-antd/grid';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { OpportunitiesComponent } from './opportunities.component';

@NgModule({
  declarations: [OpportunitiesComponent],
  imports: [CommonModule, NzGridModule]
})
export class OpportunitiesModule {}
