import { NzTimelineModule } from 'ng-zorro-antd/timeline';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ProcessComponent } from './process.component';

@NgModule({
  declarations: [ProcessComponent],
  imports: [CommonModule, NzTimelineModule]
})
export class ProcessModule {}
