import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ComparisonComponent } from './comparison/comparison.component';
import { ChoicesComponent } from './choices.component';
import { HowComponent } from './how/how.component';

@NgModule({
  declarations: [ChoicesComponent, HowComponent, ComparisonComponent],
  imports: [CommonModule, NzDropDownModule, NzIconModule]
})
export class ChoicesModule {}
