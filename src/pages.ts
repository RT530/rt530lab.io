import { IconDefinition } from '@ant-design/icons-angular';
import { NgModule, Type } from '@angular/core';
import { Routes } from '@angular/router';
import {
  QuestionCircleOutline,
  UnorderedListOutline,
  FileSearchOutline,
  RadarChartOutline,
  LineChartOutline,
  WarningOutline,
  HomeOutline,
  TeamOutline,
  DownOutline
} from '@ant-design/icons-angular/icons';

import { OpportunitiesComponent } from './app/opportunities/opportunities.component';
import { OpportunitiesModule } from './app/opportunities/opportunities.module';
import { ReferencesComponent } from './app/references/references.component';
import { ReferencesModule } from './app/references/references.module';
import { ChoicesComponent } from './app/choices/choices.component';
import { ProcessComponent } from './app/process/process.component';
import { ChoicesModule } from './app/choices/choices.module';
import { ProcessModule } from './app/process/process.module';
import { TopicComponent } from './app/topic/topic.component';
import { RisksComponent } from './app/risks/risks.component';
import { HomeComponent } from './app/home/home.component';
import { TeamComponent } from './app/team/team.component';
import { TopicModule } from './app/topic/topic.module';
import { RisksModule } from './app/risks/risks.module';
import { TeamModule } from './app/team/team.module';

interface Page {
  header: string;
  menu: string;
  path: string;
  icon: IconDefinition;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  component: any;
  module?: Type<NgModule>;
}

const pages: Page[] = [
  {
    header: 'CTiS: Impact of Technology Site',
    path: '/',
    menu: 'Home',
    icon: HomeOutline,
    component: HomeComponent
  },
  {
    header: 'Online Learning – Communication and Security Issues',
    path: '/topic',
    menu: 'Topic',
    icon: FileSearchOutline,
    component: TopicComponent,
    module: TopicModule
  },
  {
    header: 'Technology Opportunities',
    path: '/opportunities',
    menu: 'Opportunities',
    icon: RadarChartOutline,
    component: OpportunitiesComponent,
    module: OpportunitiesModule
  },
  {
    header: 'Technology Risks',
    path: '/risks',
    menu: 'Risks',
    icon: WarningOutline,
    component: RisksComponent,
    module: RisksModule
  },
  {
    header: 'Technology Choices',
    path: '/choices',
    menu: 'Choices',
    icon: QuestionCircleOutline,
    component: ChoicesComponent,
    module: ChoicesModule
  },
  {
    header: 'References',
    path: '/references',
    menu: 'References',
    icon: UnorderedListOutline,
    component: ReferencesComponent,
    module: ReferencesModule
  },
  {
    header: 'Project Process',
    path: '/process',
    menu: 'Process',
    icon: LineChartOutline,
    component: ProcessComponent,
    module: ProcessModule
  },
  {
    header: 'Team Formation Form',
    path: '/team',
    menu: 'Team Formation',
    icon: TeamOutline,
    component: TeamComponent,
    module: TeamModule
  }
];

const headerMap: Map<string, string> = new Map<string, string>();
const icons: IconDefinition[] = [DownOutline];
const routes: Routes = [];

for (const page of pages) {
  headerMap.set(page.path, page.header);
  icons.push(page.icon);
  routes.push({
    path: page.path.slice(1, page.path.length),
    component: page.component
  });
}

export { pages, icons, routes, headerMap };
