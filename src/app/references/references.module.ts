import { NzQRCodeModule } from 'ng-zorro-antd/qr-code';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ReferencesComponent } from './references.component';

@NgModule({
  declarations: [ReferencesComponent],
  imports: [CommonModule, NzQRCodeModule]
})
export class ReferencesModule {}
