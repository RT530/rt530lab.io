import { Component } from '@angular/core';

interface Times {
  date: string;
  time: string;
}

interface Member {
  lastName: string;
  firstName: string;
  studentID: number;
}

@Component({
  selector: 'team',
  templateUrl: './team.component.html'
})
export class TeamComponent {
  meetingTimes: Times[] = [
    {
      date: '8th of May',
      time: '5pm - 6pm'
    },
    {
      date: '9th of May',
      time: '12pm - 1pm'
    },
    {
      date: '22nd of May',
      time: '5pm - 6pm'
    },
    {
      date: '28th of May',
      time: '5pm - 6pm'
    },
    {
      date: '7th of June',
      time: '1pm - 3pm'
    },
    {
      date: '8th of June',
      time: '12pm - 8pm'
    }
  ];

  teamMember: Member[] = [
    {
      lastName: 'Tsai',
      firstName: 'Ricky',
      studentID: 21153901
    },
    {
      lastName: 'Ferrera',
      firstName: 'Clyde',
      studentID: 21152645
    },
    {
      lastName: 'Tukor',
      firstName: 'Lillian',
      studentID: 21139206
    }
  ];
}
